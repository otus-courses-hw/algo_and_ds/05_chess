#include <cstddef>
#include <cstdint>
#include <utility>

namespace bitboard
{
    std::pair<uint64_t, unsigned int> king(uint64_t positions);
};
