all: test

test: test_main.o test.o bitboard.o
	g++ -g -O0 -o $@ $^

%.o: %.cpp
	g++ -g -std=c++20 -O0 -o $@ -c $<

test.o: test_main.cpp bitboard.cpp

.PHONY: clean run

clean:
	rm *.o test

run:
	./test
