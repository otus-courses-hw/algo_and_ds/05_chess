#include "bitboard.hpp"
#include <array>
#include <algorithm>
#include <ranges>

namespace bitboard {
    namespace {
        uint64_t dec2bit(uint64_t pos)
        {
            return 1ULL << pos;
        }

        unsigned int popcnt1(uint64_t value)
        {
            unsigned int counter{0};
            while (value != 0)
            {
                if (value & 1)
                    ++counter;
                value >>= 1;
            }
            return counter;
        }

        unsigned int popcnt2(uint64_t value)
        {
            unsigned int counter{0};
            while (value > 0)
            {
                counter++;
                value &= (value -1 );
            }
            return counter;
        }

        std::array<unsigned int, 255> init_cache()
        {
            namespace ranges = std::ranges;
            std::array<unsigned int, 255> cache;
            ranges::for_each(cache, [i=0](auto &n) mutable {n=popcnt2(i++);});
            return cache; 
        }

        auto cache = init_cache();
        unsigned int popcnt3(uint64_t value)
        {
            unsigned int counter{0};
            while (value > 0)
            {
                counter += cache[value & 255];
                value >>= 8;
            }
            return counter;
        }
    }

    std::pair<uint64_t, unsigned int> king(uint64_t pos)
    {
        pos = dec2bit(pos);
        uint64_t left_board_mask = 0xfefefefefefefefe;
        uint64_t right_board_mask = 0x7f7f7f7f7f7f7f7f;
        uint64_t tweak_left_board = pos & left_board_mask;
        uint64_t tweak_right_board = pos & right_board_mask;
        auto value =  (pos & tweak_left_board) << 7 | pos << 8 | tweak_right_board << 9 |
                      (pos & tweak_left_board) >> 1 |            tweak_right_board << 1 |
                      (pos & tweak_left_board) >> 9 | pos >> 8 | tweak_right_board >> 7;

        return std::make_pair(value, popcnt3(value));
    }
}

