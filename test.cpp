//#include "test_main.cpp"
#include <catch.hpp>
#include <utility>

#include "bitboard.hpp"

TEST_CASE("king")
{
    REQUIRE(bitboard::king(0) == std::make_pair(770UL, 3U));
    REQUIRE(bitboard::king(1) == std::make_pair(1797UL, 5U));
    REQUIRE(bitboard::king(7) == std::make_pair(49216UL, 3U));
    REQUIRE(bitboard::king(8) == std::make_pair(197123UL, 5U));
    REQUIRE(bitboard::king(10) == std::make_pair(920078UL, 8U));
    REQUIRE(bitboard::king(15) == std::make_pair(12599488UL, 5U));
    REQUIRE(bitboard::king(54) == std::make_pair(16186183351374184448UL, 8U));
    REQUIRE(bitboard::king(55) == std::make_pair(13853283560024178688UL, 5U));
    REQUIRE(bitboard::king(56) == std::make_pair(144959613005987840UL, 3U));
    REQUIRE(bitboard::king(63) == std::make_pair(4665729213955833856UL, 3U));
}
